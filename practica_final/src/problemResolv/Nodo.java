package problemResolv;


public class Nodo{

	private Nodo parent;
	private Operador accion;
	private int coste;
	private int x,y;
	
	private void checkInvariant(){
		assert this.x > 0;
		assert this.y > 0;
		assert this.x < 11;
		assert this.y < 11;
	}
	

	/**
	 * @param x
	 * @param y
	 * @param parent
	 * @param accion
	 * @param coste
	 */
	public Nodo(int x, int y, Nodo parent, Operador accion,
			int coste) {
		super();
		this.x = x;
		this.y = y;
		this.parent = parent;
		this.accion = accion;
		this.coste = coste;
		this.checkInvariant();
	}




	/**
	 * @return the parent
	 */
	public Nodo getParent() {
		return parent;
	}

	/**
	 * @return the accion
	 */
	public Operador getAccion() {
		return accion;
	}

	/**
	 * @return the coste
	 */
	public int getCoste() {
		return coste;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}



	public int profundidad() {
		Nodo nodo = this;
		int prof=0;
		
		while(nodo.parent != null){
			prof++;
			nodo = nodo.parent;
		}
		
		return prof;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[" + x + ", " + y +"]";
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return x + 10 * y;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nodo other = (Nodo) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	
	
}
