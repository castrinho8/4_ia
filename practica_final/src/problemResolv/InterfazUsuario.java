package problemResolv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import problemResolv.estrategias.*;

public class InterfazUsuario {
	
	private enum Penalizacion{NONE, UNICA, ELEGIR }


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Problema p;		
		
		final Casilla cinicial = getCasInicial();
		final Casilla cfinal = getCasFinal();
		HashMap<Casilla,Integer> cpenalizacion = getCasPenalizacion(cinicial);
		Estrategia estra = getEstrategia();
		FuncionHeuristica funcHeurist = (estra == AEstrella.getInstance()) ? getHeuristica(cfinal) : null;		
		FuncionMeta funcionmeta = new FuncionMeta(){

			@Override
			public boolean check(Nodo n) {
				return ((Nodo) n).getX() == cfinal.x && ((Nodo) n).getY() == cfinal.y;
				
			}
			
		};
		
		ArrayList<Operador> operadores = getOperadores(cpenalizacion);	
		
		
		Nodo nodoInicial = new Nodo(cinicial.x, cinicial.y, null, null, 0);
				
		p = new Problema(nodoInicial, estra, operadores, funcionmeta,funcHeurist);
		
		ArrayList<Nodo> x = p.solve();
		
		System.out.println("\n\nSolucion:");
		
		if (x==null){
			System.out.println("Sin solución.");
		}else{
			for(Nodo n : x)
				System.out.println(n);
				
		}
		
	}

	private static FuncionHeuristica getHeuristica(final Casilla cfinal) {
		System.out.println("Seleccione entre una de las dos funciones heuristicas propuestas:\n\tA)Distancia vertical.\n\tB)Distancia de Manhattan.");
	
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
	
		try {
			while(true){
				System.out.print("\tSeleccione unha letra adecuada (A ou B):");
				s = reader.readLine();
				if(s.equals("A")){
					return new FuncionHeuristica(){

						@Override
						public int valorHeuristica(Nodo n) {
							return (Math.abs(n.getY() - cfinal.y))/2;
						}
						
					};
				}else if(s.equals("B")){
					return new FuncionHeuristica(){

						@Override
						public int valorHeuristica(Nodo n) {
							return (Math.abs(n.getX() - cfinal.x) + Math.abs(n.getY() - cfinal.y))/3;
						}
						
					};
				}
			}
		}catch(IOException e){
			System.err.println("Erro recuperando datos.");
			System.exit(-1);
		}
		assert false;
		return null;
		
	
	}

	private static Estrategia getEstrategia(){
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		
		System.out.println("Seleccione a estratexia a seguir.\n\tA) Para Busqueda A*.\n\tB) Para busqueda" +
				   " en Profundidade.\n\tC) Para buscar en Anchura.");

		try {
			while(true){
				System.out.print("\tSeleccione una letra adecuada (A, B o C):");
				s = reader.readLine();
				if(s.equals("A")){
					return AEstrella.getInstance();
				}else if(s.equals("B")){
					return Profundidad.getInstance();
				}else if(s.equals("C")){
					return Anchura.getInstance();
				}
			}
		}catch(IOException e){
			System.err.println("Erro recuperando datos.");
			System.exit(-1);
		}
		assert false;
		return null;
	}

	private static HashMap<Casilla, Integer> getCasPenalizacion(Casilla cinicial) {
		
		Penalizacion penalizacion;
		HashMap<Casilla,Integer> casillaspenalizacion = new HashMap<Casilla,Integer>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		int x = 0,y = 0,value=0,numero=0;
		boolean enteronovalido;
		Casilla casilla;
		
		System.out.println("\nCasillas con penalización\n\tA)Non hai casillas con penalizacion.\n\tB)" + 
				"Seleccionar casillas con penalización e o valor de esta.\n\tC)Seleccionar"+
				" número de casillas con penalización e o valor (único) desta penalización.");

		try{
			
			
			while(true){
				System.out.print("\tSeleccione una letra adecuada (A, B o C):");
				s = reader.readLine();
				if(s.equals("A")){
					penalizacion = Penalizacion.NONE;
					break;
				}else if(s.equals("B")){
					penalizacion = Penalizacion.ELEGIR;
					break;
				}else if(s.equals("C")){
					penalizacion = Penalizacion.UNICA;
					break;
				}
			}

		switch(penalizacion){
		case ELEGIR:
			while(true){
				do{
					System.out.println("Desexa engadir unha nova casilla con penalizacion?[S]/N");
					s = reader.readLine();
				}while(! (s.equals("S") || s.equals("N") || s.equals("")));
				
				if(s.equals("N")){
					break;
				}else{
		
					do{
						System.out.print("\tSeleccione a posición horizontal da casilla:");
						s = reader.readLine();
						try{
							x = Integer.parseInt(s);
							enteronovalido = false;
						}catch(NumberFormatException e){
							enteronovalido = true;
						}
						
					}while(enteronovalido || x < 1 || x > 10);
					
					
					do{
						System.out.print("\tSeleccione a posición vertical da casilla:");
						s = reader.readLine();
						try{
							y = Integer.parseInt(s);
							enteronovalido = false;
						}catch(NumberFormatException e){
							enteronovalido = true;
						}
						
					}while(enteronovalido || y < 1  || y > 10);
		
					
					do{
						System.out.print("\tSeleccione o valor da penalización:");
						s = reader.readLine();
						try{
							value = Integer.parseInt(s);
							enteronovalido = false;
						}catch(NumberFormatException e){
							enteronovalido = true;
						}
						
					}while(enteronovalido);
		
					casilla = new Casilla(x,y);
					
					if(casilla.equals(cinicial)){
						System.out.println("A casilla inicial non pode ter penalización.");
					}else{ 
						casillaspenalizacion.put(casilla, value);
					}
				}
			}
			break;
		case UNICA:
			do{
				System.out.print("\tSeleccione o numero de casillas con penalizacion [0-99] :");
				s = reader.readLine();
				try{
					numero = Integer.parseInt(s);
					enteronovalido = false;
				}catch(NumberFormatException e){
					enteronovalido = true;
				}
			}while(enteronovalido || numero < 0  || numero > 99);

			do{
				System.out.print("\tSeleccione o valor da penalización :");
				s = reader.readLine();
				try{
					value = Integer.parseInt(s);
					enteronovalido = false;
				}catch(NumberFormatException e){
					enteronovalido = true;
				}
			}while(enteronovalido);

			while(casillaspenalizacion.size() != numero){
				x = 1 + (int)(Math.random()*10);
				y = 1 + (int)(Math.random()*10);
				
				if(x == cinicial.x && y == cinicial.y)
					continue;
				
				casillaspenalizacion.put(new Casilla(x,y), value);
				
			}	
			
			break;	
		case NONE:
			break;
		}
		}catch(IOException e){
			System.err.println("Erro recuperando datos.");
			System.exit(-1);
		}
		return casillaspenalizacion;
	}

	private static Casilla getCasFinal() {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		int x = 0,y = 0;
		boolean enteronovalido;
		
		
		System.out.println("\nPosicion Final.");
		
		try{
			
			do{
				System.out.print("\tSeleccione a posición horizontal final:");
				s = reader.readLine();
				try{
					x = Integer.parseInt(s);
					enteronovalido = false;
				}catch(NumberFormatException e){
					enteronovalido = true;
				}
				
			}while(enteronovalido || x < 1 || x > 10);
			
			do{
				System.out.print("\tSeleccione a posición vertical final:");
				s = reader.readLine();
				try{
					y = Integer.parseInt(s);
					enteronovalido = false;
				}catch(NumberFormatException e){
					enteronovalido = true;
				}
				
			}while(enteronovalido || y < 1  || y > 10);
			
			return new Casilla(x,y);
			
		}catch(IOException e){
			System.err.println("Erro recuperando datos.");
			System.exit(-1);
		}
		
		return null;
	}

	private static Casilla getCasInicial() {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		int x = 0,y = 0;
		boolean enteronovalido;
		
		try{
			System.out.println("Posicion de partida.");
			do{
				System.out.print("\tSeleccione a posición horizontal de partida:");
				s = reader.readLine();
				try{
					x = Integer.parseInt(s);
					enteronovalido = false;
				}catch(NumberFormatException e){
					enteronovalido = true;
				}
				
			}while(enteronovalido || x < 1 || x > 10);
			
			do{
				System.out.print("\tSeleccione a posición vertical de partida:");
				s = reader.readLine();
				try{
					y = Integer.parseInt(s);
					enteronovalido = false;
				}catch(NumberFormatException e){
					enteronovalido = true;
				}
				
			}while(enteronovalido || y < 1 || y > 10);
		
			return new Casilla(x,y);
		}catch(IOException e){
			System.err.println("Erro recuperando datos.");
			System.exit(-1);
		}
		return null;
	}

	private static ArrayList<Operador> getOperadores(final HashMap<Casilla, Integer> cpenalizacion){
		
		ArrayList<Operador> a = new ArrayList<Operador>();
		
		
		
		//Arriba   e Dereita  ( 1,2)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() <= 9 && n.getY() <= 8;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()+1,n.getY()+2);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()+1,n.getY()+2,n,this, n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Arriba   e Esquerda (-1,2)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() >= 2 && n.getY() <= 8;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()-1,n.getY()+2);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()-1,n.getY()+2,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Abaixo   e Dereita  (1,-2)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() <= 9 && n.getY() >= 3;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);

				Casilla c = new Casilla(n.getX()+1,n.getY()-2);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()+1,n.getY()-2,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Abaixo   e Esquerda (-1,-2)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() >= 2 && n.getY() >= 3;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()-1,n.getY()-2);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()-1,n.getY()-2,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Dereita  e Arriba   (2,1)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() <= 8 && n.getY() <= 9;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()+2,n.getY()+1);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()+2,n.getY()+1,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Dereita  e Abaixo   (2,-1)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() <= 8 && n.getY() >= 2;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()+2,n.getY()-1);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()+2,n.getY()-1,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Esquerda e Arriba   (-2,1)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() >= 3 && n.getY() <= 9;  
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()-2,n.getY()+1);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()-2,n.getY()+1,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		//Esquerda e Abaixo   (-2,-1)
		a.add(new Operador(){

			@Override
			public boolean precondicion(Nodo n) {
				return n.getX() >= 3 && n.getY() >= 2;
			}

			@Override
			public Nodo ejec(Nodo n) {
				assert precondicion(n);
				Casilla c = new Casilla(n.getX()-1,n.getY()-2);
				int costecasilla = cpenalizacion.get(c) == null ? 0 : cpenalizacion.get(c);
				return new Nodo(n.getX()-1,n.getY()-2,n,this,n.getCoste() + costecasilla + 1);
			}
			
		});
		
		
		return a;
	}
	
}
