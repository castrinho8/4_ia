package problemResolv;

public interface Operador {

	public boolean precondicion(Nodo n);
	public Nodo  ejec(Nodo n);
}
