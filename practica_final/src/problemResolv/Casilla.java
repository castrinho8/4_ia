package problemResolv;

public class Casilla {
	public int x, y;

	public Casilla(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Casilla))
			return false;
		
		Casilla cas = (Casilla)  obj;	
		if (cas.x != this.x)
			return false;
		
		if (cas.y != this.y)
			return false;
		
		return true;
		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.x + 10 * this.y;
	}
	
	
	
}
