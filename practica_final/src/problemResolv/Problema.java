package problemResolv;

import java.util.ArrayList;
import java.util.Iterator;

import problemResolv.estrategias.*;

public class Problema {

	private Nodo nodoInicial;
	private ArrayList<Operador> operaciones;
	private FuncionMeta funcionMeta;
	private Estrategia estrategia;
	private FuncionHeuristica funcionHeuristica;
	
	/**
	 * @param nodoInicial
	 * @param nodoFinal
	 * @param estrategia
	 * @param arrayList
	 * @param funcionMeta
	 * @param funcHeurist 
	 */
	public Problema(Nodo nodoInicial, Estrategia estrategia, 
			ArrayList<Operador> arrayList, FuncionMeta funcionMeta, FuncionHeuristica funcHeurist) {
		this.nodoInicial = nodoInicial;
		this.estrategia = estrategia;
		this.operaciones = arrayList;
		this.funcionMeta = funcionMeta;
		this.funcionHeuristica = funcHeurist;
	}


	public void cambiarEstrategia(Estrategia e){
		this.estrategia = e;
	}

	public ArrayList<Nodo> solve() {
		return estrategia.solve(this);
	}
	
//GETTERS
	public Nodo get_nodoInicial(){
		return this.nodoInicial;
	};
	
	public Estrategia get_estrategia(){
		return this.estrategia;
	};
	
	public ArrayList<Operador> get_operaciones(){
		return this.operaciones;
	};
	
	public FuncionMeta get_funcionMeta(){
		return this.funcionMeta;
	};
	
	
	public FuncionHeuristica getFuncionHeuristica() {
		return funcionHeuristica;
	}


	public ArrayList<Operador> get_op_validas(Nodo nodo){
		
		ArrayList<Operador> op_validas = new ArrayList<Operador>();
    	
    	for(Operador act : this.get_operaciones())
    		if(act.precondicion(nodo))
				op_validas.add(act);
 		return op_validas;
	};
}
