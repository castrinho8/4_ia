package problemResolv.estrategias;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import problemResolv.Nodo;
import problemResolv.Operador;
import problemResolv.Problema;

public class Profundidad implements Estrategia {
	
	private static Profundidad instance;
	
	private Profundidad() {
		super();
	}

	
	@Override
	public ArrayList<Nodo> solve(Problema p) {
		int i = 0,j;
		Nodo nodo_actual, new_node;
		Stack<Nodo> frontera_pila = new Stack<Nodo>();
		Set<Nodo> explorados = new HashSet<Nodo>();
		
		frontera_pila.add(p.get_nodoInicial());
		
		System.out.println("HOLA!!!");
		
		if(p.get_funcionMeta().check(p.get_nodoInicial()))
			return get_path(p.get_nodoInicial());
		
		while(!frontera_pila.isEmpty()){
			nodo_actual = frontera_pila.pop();
			explorados.add(nodo_actual);
			
			System.out.print("\n Iteración " + i++ + ":\n\tExplorando Nodo " + nodo_actual.toString() + "\n\tCamino Actual: ");
			
			new_node = nodo_actual;
			do{
				System.out.print(new_node + "<--");
				new_node = new_node.getParent();
			}while(new_node != null);
			System.out.print("NULL");
			
			System.out.print("\n\tNodos Abiertos: ");
			
			for(Nodo n : frontera_pila){
				System.out.print(n + ", ");
			}
			
			System.out.print("\n\tNuevos descendientes: ");
			j = frontera_pila.size();
			
			for(Operador oper : p.get_op_validas(nodo_actual)){
				new_node = oper.ejec(nodo_actual);
				
				if(explorados.contains(new_node) || frontera_pila.contains(new_node))
					continue;
				
				frontera_pila.push(new_node);
				System.out.print(new_node.toString() + ", ");
					
				if(p.get_funcionMeta().check(new_node))
					return get_path(new_node);
			}
			
			if(frontera_pila.size() == j){
				System.out.print("\n\tBACTRACKING");
			}
		}
		
		return null;
		
	}
	
	
	private ArrayList<Nodo> get_path(Nodo nodo_actual) {
		ArrayList<Nodo> path = new ArrayList<Nodo>();
		Nodo nodo = nodo_actual;
		
		while(nodo != null){
			path.add(nodo);
			nodo = nodo.getParent();
		}
		
		return path;
	}
	

	/*
	
	
	@Override
	public ArrayList<NodoXadrez> solve(Problema p) {
		// TODO Auto-generated method stub
		ArrayList<NodoXadrez> camino = new ArrayList<NodoXadrez>();
	    Stack<NodoXadrez> pila = new Stack<NodoXadrez>();
		ArrayList<Operador> operadores_actuales = new ArrayList<Operador>();

	    camino.add(p.get_nodoInicial());
	    pila.push(p.get_nodoInicial());
	    
	    while(true){
	    	if(p.get_funcionMeta().check(pila.get(0))){	
	    		return camino;
	    	}
	    	
	    	operadores_actuales = p.get_op_validas(pila.get(0));
	    	
	    	while(operadores_actuales.iterator().hasNext()){
				NodoXadrez new_nodo = null;// operadores_actuales.iterator().next();//Escoge el operador
			    camino.add(pila.get(0));//Añade el nuevo nodo al camino
	    		pila.push(new_nodo);//Añade el nuevo nodo a la pila
	    		operadores_actuales.clear();//Borra las operaciones actuales.
		    	operadores_actuales = p.get_op_validas(pila.get(0));//Actualiza las operaciones al nuevo nodo
	    	}
	    	
	    	if(p.get_funcionMeta().check(pila.get(0)))
	    		return camino;
	    	else
	    		camino.remove(pila.get(0));
	    		pila.pop();
	    }
	    /*
		meter nodo ini en pila
		while(){
			if(PM(ultimo nodo)) return ultimo nodo
					
			while(haya operadores aplicables al ultimo nodo){
				escoger operador y aplicarlo
				meter nuevo nodo en pila
			}
			if(PM(ultimo nodo))
				return ultimo nodo
			else
				quitar ultimo nodo
		}	
	}
*/
	
	public static Estrategia getInstance() {
		if (instance == null)
			instance = new Profundidad();
		
		return instance;
	}

}
