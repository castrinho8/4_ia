package problemResolv.estrategias;

import java.util.ArrayList;

import problemResolv.Nodo;
import problemResolv.Problema;

public interface Estrategia {
	
	public ArrayList<Nodo> solve(Problema p);
	
}
