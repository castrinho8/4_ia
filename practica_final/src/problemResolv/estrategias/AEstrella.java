package problemResolv.estrategias;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import problemResolv.FuncionHeuristica;
import problemResolv.Nodo;
import problemResolv.Operador;
import problemResolv.Problema;

public class AEstrella implements Estrategia {

	private static AEstrella instance;
	
	private AEstrella(){
		super();
	}

	public ArrayList<Nodo> solve(final Problema p) {
		
		Nodo nodo_actual, new_node;
		int i = 0;
		PriorityQueue<Nodo> frontera = new PriorityQueue<Nodo>(50, new Comparator<Nodo>(){

			@Override
			public int compare(Nodo arg0, Nodo arg1) {
				int costeNodo1;
				int costeNodo2;
				
				costeNodo1 = arg0.getCoste() + p.getFuncionHeuristica().valorHeuristica(arg0);
				costeNodo2 = arg1.getCoste() + p.getFuncionHeuristica().valorHeuristica(arg1);
				
				return new Integer(costeNodo1).compareTo(costeNodo2);
			}
		});
		Set<Nodo> explorados = new HashSet<Nodo>();
				
		if(p.get_funcionMeta().check(p.get_nodoInicial()))
			return get_path(p.get_nodoInicial());
		
		frontera.offer(p.get_nodoInicial());
		
		while(!frontera.isEmpty()){
			System.out.print("\nIteración " + i++ + ":\n\tNodos Abiertos:");
			
			for(Nodo n : frontera){
				printNodo(n,p.getFuncionHeuristica());
				System.out.print(", ");
			}
			
			nodo_actual = frontera.poll();
			explorados.add(nodo_actual);
			
		
			System.out.print("\n\tExplorando Nodo ");
			printNodo(nodo_actual,p.getFuncionHeuristica());
			
			System.out.print("\n\tCamino hasta nodo:");
			
			new_node = nodo_actual;
			while(new_node != null){
				printNodo(new_node,p.getFuncionHeuristica());
				System.out.print(" <- ");
				new_node = new_node.getParent();
			}
			
			if(p.get_funcionMeta().check(nodo_actual)){
				System.out.println("SOLUCIÓN!!\nCoste de la busqueda:" + explorados.size()+ "\nNodos analizados:" + new Integer(explorados.size() + frontera.size()));
				return get_path(nodo_actual);
			}	
			
			
			System.out.print("\n\tNodos Abiertos: ");
			
			for(Operador oper : p.get_op_validas(nodo_actual)){
				new_node = oper.ejec(nodo_actual);
				
				if(explorados.contains(new_node) || frontera.contains(new_node))
					continue;
				
				frontera.add(new_node);
				printNodo(new_node,p.getFuncionHeuristica());
				System.out.print(", ");
			}			
		}
		
		return null;
		
	}
	
	
	private void printNodo(Nodo n, FuncionHeuristica h){
		System.out.print("(" + n.toString() + ", " + n.getCoste() + ", " + h.valorHeuristica(n) + ")");
	}

	private ArrayList<Nodo> get_path(Nodo nodo_actual) {
		ArrayList<Nodo> path = new ArrayList<Nodo>();
		Nodo nodo = nodo_actual;
		
		while(nodo != null){
			path.add(nodo);
			nodo = nodo.getParent();
		}
		
		return path;
	}
	
	
	public static Estrategia getInstance() {
		if(instance == null)
			instance = new AEstrella();
		return instance;
		
	}

}
