package problemResolv.estrategias;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import problemResolv.Nodo;
import problemResolv.Operador;
import problemResolv.Problema;

public class Anchura implements Estrategia {

	private static Estrategia instance;
	
	private Anchura(){
		super();
	}
	
	/* (non-Javadoc)
	 * @see Estrategia#solve()
	 */
	@Override
	public ArrayList<Nodo> solve(Problema p) {
		int i=0;
		Nodo nodo_actual, new_node;
		Queue<Nodo> frontera_cola = new LinkedList<Nodo>();
		Set<Nodo> explorados = new HashSet<Nodo>();
		
		frontera_cola.add(p.get_nodoInicial());
		
		if(p.get_funcionMeta().check(p.get_nodoInicial()))
			return get_path(p.get_nodoInicial());
		
		while(!frontera_cola.isEmpty()){
			nodo_actual = frontera_cola.poll();
			explorados.add(nodo_actual);
			
			System.out.print("\nIteración "+ i++ +":\n\tExplorando Nodo (" + nodo_actual + ")\n\tProfundidad: "
					+ nodo_actual.profundidad() + "\n\tNodos Abiertos: ");
			
			for(Operador oper : p.get_op_validas(nodo_actual)){
				new_node = oper.ejec(nodo_actual);
				
				if(!frontera_cola.contains(new_node) && !explorados.contains(new_node)){
						frontera_cola.add(new_node);
						System.out.print("("+ new_node + "), ");
				}
					
				if(p.get_funcionMeta().check(new_node)){
					System.out.println("\nCoste de la busqueda:" + explorados.size()+ "\nNodos abiertos:" + new Integer(explorados.size() + frontera_cola.size()));
					return get_path(new_node);
				}
			}			
		}
		
		//No se atopou solución!!
		return null;
		
	}

	
	private ArrayList<Nodo> get_path(Nodo nodo_actual) {
		ArrayList<Nodo> path = new ArrayList<Nodo>();
		Nodo nodo = nodo_actual;
		
		while(nodo != null){
			path.add(nodo);
			nodo = nodo.getParent();
		}
		
		return path;
	}

	public static Estrategia getInstance() {
		if(instance == null){
			instance = new Anchura();
		}
		return instance;
	}

}
