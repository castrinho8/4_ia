\documentclass[a4paper,12pt,titlepage]{article}

% Se cargan los paquetes necesarios
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{helvet}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{verbatim}
\usepackage{indentfirst}
\usepackage{color}
\usepackage[x11names]{xcolor}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{wasysym}
\usepackage{pstricks}

\setlength{\hoffset}{0pt}

\hypersetup{
  colorlinks=true,
  linkcolor=blue
}

\begin{document}

\title{\Huge{Boletín 3: Control difuso del Péndulo Invertido. Análisis Crítico} \\ \small{Intelixencía Artificial}}
\author{Pablo Castro Valiño \\ \small{(pablo.castro1@udc.es)}
  \and  Marcos Chavarría Teijeiro \\ \small{(marcos.chavarria@udc.es)}}
\date{2012-2013}


\maketitle

\tableofcontents

\newpage
\section{El problema: control del péndulo invertido}

\subsection{Péndulo invertido}
Un péndulo es un sistema físico que esta formada por una masa suspendida de un pivote y que puede oscilar libremente bajo la acción gravitatoria u otra característica física. La masa esta unida con el pivote o punto de apoyo a través de una barra. Esta barra ejecuta movimientos alternados en torno a una posición central.

Cuando dicha barra es rígida y el punto de apoyo esta debajo de la masa hablamos de un péndulo invertido. En la Figura~\ref{fig:figurapendulo} podemos ver un diagrama del modelo.

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{pendulo}
\caption{Diagrama problema Péndulo Invertido}
\label{fig:figurapendulo}
\end{figure}

En la figura anterior podemos ver un cuerpo de masa $m$ unido con una plataforma móvil a través de una barra rígida de longitud $l$. Esta barra esta desplazada con respecto a la vertical un angulo $\theta$. Esta es una de las implementaciones más habituales del problema, en la que se usa como punto de apoyo una anden o plataforma móvil.

Por otro lado también es importante tener en cuenta que el péndulo solo tiene un grado de libertad, es decir solo se le permite moverse en un sentido y en el sentido contrario y no en todo el plano.

Otra de las posibilidades a tener en cuenta cuando hablamos de péndulos invertidos es en vez de intentar re-equilibrar la masa con movimientos horizontales, hacerlo con movimientos verticales. Esto se puede apreciar en la Figura~\ref{fig:kapitza}.

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{kapitza.png}
\caption{Diagrama del Péndulo de Kapitza}
\label{fig:kapitza}
\end{figure}

Esta segunda aproximación se denomina péndulo de Kapitza en honor al premio Nobel de física ruso Pyotr Kapitza, que trabajo en explicar el funcionamiento de este tipo de péndulos.

En las siguientes secciones se describirá métodos para controlar péndulos invertidos como los de la primera aproximación.

\subsection{Control del péndulo invertido}
El problema consiste en controlar la velocidad de la plataforma móvil de forma que péndulo se mantenga en equilibrio, es decir que el angulo de la barra con respecto a la vertical sea $\theta = 0$.

Las variables a tener en cuenta en el problema son, el angulo con respecto a la vertical, la velocidad angular de la masa y la velocidad con la que se mueve el anden.

La idea que subyace es que si la barra se esta cayendo hacia un lado con una cierta velocidad angular si desplazamos la plataforma móvil hacia ese mismo lado con una cierta velocidad, debido a la inercia del peso, el péndulo se re-equilibrará.

\subsection{Utilidad practica}

El control de un péndulo invertido es un problema clásico de dinámica y de teoría de control. Es usado habitualmente para hacer \emph{benchmarking} en para algoritmos e control como redes neuronales, control difuso, etc.

Fuera del ámbito académico e investigador se usa para control de cohetes y misiles entre otras cosas. Se puede resaltar que es el principal elemento de control en los transportadores personales de tipo \emph{Segway} como el que se puede ver en la Figura~\ref{fig:segway}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.4\textwidth]{segway.jpg}
  \caption{Trasportador Personal Segway}
  \label{fig:segway}
\end{figure}

Estos transportadores son vehículos de transporte de dos ruedas que cuentan con auto-balanceo,  el control de este auto-balanceo es un problema equivalente al del péndulo invertido.

\newpage
\section{Aproximación seguida en la documentación}
La documentación aportada utiliza un mecanismo de control difuso. Antes de empezar a concretar detalles sobre la implementación explicaremos que es la lógica difusa y que son los conjuntos difusos.

\subsection{Lógica difusa y conjuntos difusos}
La teoría de conjuntos difusos surge en Estados Unidos en el año 1965 a través del trabajo de Lofti A. Zadeh. Permite representar matemáticamente la impresión inherente  a ciertos tipos de conjuntos.

Si trabajamos con conjuntos convencionales podemos establecer una función grado de pertenencia que indica si un elemento del dominio pertenece o no al conjunto. De esta forma si tenemos el conjunto formado por los elementos $\{1, 2, 3\}$ la función grado de pertenencia de dicho conjunto devolvería 1 para esos valores y 0 para los demás.

No obstante hay ciertos dominios en los que no es tan sencillo ni tan practico hacer una clasificación tan categórica. Decir que personas pertenecen al conjunto de personas altas es algo que depende de aspectos tanto culturales como contextules.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth]{logicadifusa}
  \caption{Funciones grado de pertenencia en conjuntos convencionales y difusos}
  \label{fig:logicadifsusa}
\end{figure}

De esta forma los conjuntos difusos relajan la función grado de pertenencia de forma que en vez de tomar valores en el conjunto $\{0, 1\}$ los toman en el intervalo $[0,1]$. De esta forma un elemento que al aplicarle la función grado de pertenencia da como 0 sabemos claramente que no pertenece al conjunto. Si la función grado de pertenencia devuelve 1 sabemos con claridad que si que pertenece al conjunto. En caso de devolver un valor entre $0$ y $1$, sabremos que tiene características de los elementos que pertenecen y los que no pertenecen al conjunto.

El conjunto de elementos que sabemos con claridad que pertenecen al conjunto, es decir que la función grado de pertenencia devuelve 1 se denomina núcleo del conjunto difuso y equivale al concepto de conjunto no difuso (categórico).

La lógica difusa permite trabajar con este tipo de conjuntos. De esta forma podremos trabajar con reglas del tipo si $a$ es $X$ entonces $b$ es $Y$ donde $X$ e $Y$ son conjuntos difusos. Sabiendo el grado de pertenencia de $a$ a $X$ podríamos calcular el grado de pertenecía de $b$ a $Y$.

\subsection{Control difuso}
El control difuso es un sistema de control que utiliza lógica difusa. Tiene la ventaja frente a otros tipos de controles (redes neuronales, algoritmos genéticos, etc.) de que se puede expresar en términos y reglas que son fácilmente entendibles por humanos además en caso de que se pretenda mecanizar tareas que están siendo llevadas a cabo por humanos, el proceso es mucho más fácil.

En la Figura~\ref{fig:controldifuso} podemos ver un diagrama de bloques de un sistema de control difuso convencional.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth]{controldifuso}
  \caption{Diagrama de bloques de un sistema de control difuso}
  \label{fig:controldifuso}
\end{figure}

Los componenentes fundamentales de estos sistemas son:

\paragraph{Sensores} Los sensores captura las características físicas del sistema a controlar. Generalmente la salida de estos sistemas es analógica por lo que sera necesaria una conversión.

\paragraph{Normalización} La salida de los sensores que miden las variables a controlar del sistema generalmente aportan una salida analógica por lo que será necesaria una normalización.

\paragraph{\emph{Fuzzyficación}} Una vez obtenidos valores normalizados deberá especificarse en que medida pertenecen o no a los conjuntos del modelo. Este procedimiento se denomina fuzzyfication y consiste en aplicar la función de grado de pertenencia de cada conjunto difuso al valor obtenido.

\paragraph{Motor de inferencia} Este componente será el encargado de producir la salida del sistema a partir de la entrada en forma de el grado de pertenencia a cada uno de los conjuntos del sistema y una base de reglas. Esta base de reglas consiste en un conjunto de normas de tipo \emph{if-then}

\paragraph{\emph{Defuzzyficación}} La salida del motor de inferencia es en forma de la pertenencia o no a otros conjuntos que pueden y suelen estar en un dominio diferente al de la entrada. En la Figura~\ref{fig:defuzzyfication} podemos ver una salida tipo de un sistema de control difuso.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth]{defuzzyfication}
  \caption{Diagrama de bloques de un sistema de control difuso}
  \label{fig:defuzzyfication}
\end{figure}

Existen varios métodos para obtener un valor único a partir de la salida anterior. Podemos citar el centro del área, centro de gravedad, media de los máximos (mean of maxima), media difusa, etc.

Uno de los más usados es el centro de gravedad que consiste en, como su propio nombre indica, calcular el centro de gravedad de la figura resultante. Esto es, "\emph{el punto respecto al cual las fuerzas que la gravedad ejerce sobre los diferentes puntos materiales que constituyen el cuerpo producen un momento resultante nulo}" \cite{centrodegravedad}.

\paragraph{Desnormalización} Una vez obtenido un valor único de la salida, tenemos que convertirlo a un valor analógico para que pueda entenderlo un mecanismo de control.

\paragraph{Control sobre sistema} Se trata del mecanismo de control que actuara sobre el sistema intentando modificar su comportamiento.



\subsection{Control difuso de un péndulo invertido}
En el caso del péndulo invertido se ha decidido que la entrada del sistema será el angulo con respecto a la vertical y la salida de este sera la velocidad que se le debe imprimir al anden. Se han dividido los valores de estas variables en cinco conjuntos difusos:

\begin{itemize}
  \item Negativo Alto (NA)
  \item Negativo Bajo (NB)
  \item Cero (ZE)
  \item Positivo Bajo (PB)
  \item Positivo Alto (PA)
\end{itemize}

Esta es una categorización bastante típica en el mundo del control difuso. En la Figura~\ref{fig:funcionpertenencia} podemos ver el valor de la función de pertenencia de dichos conjuntos difusos.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth]{funcionpertenencia}
  \caption{Funcion de pertenencia de los conjuntos difusos.}
  \label{fig:funcionpertenencia}
\end{figure}

Por ejemplo podemos ver que un valor entre 0 y a pertenecerá en parte a los conjuntos difusos Cero y Positivo Bajo (Positivo Pequeño en el diagrama) por lo que la función grado de pertenencia de estos dos conjuntos devolverá un valor entre 0 y 1 para ese valor.


Como mencionamos antes la idea que subyace es mover el anden a una velocidad similar y con la misma dirección con la que esta cayendo el péndulo. Las reglas usadas en el sistema propuesto son las siguientes:

\begin{itemize}
  \item Regla 1: Si el angulo es Negativo Alto \textbf{y} la velocidad angular es Cero \textbf{entonces} la velocidad sera Negativa Alta.
  \item Regla 2: Si el angulo es Negativo Bajo \textbf{y} la velocidad angular es Cero \textbf{entonces} la velocidad sera Negativa Baja.
  \item Regla 3: Si el angulo es Negativo Bajo \textbf{y} la velocidad angular es Positiva Baja \textbf{entonces} la velocidad sera Cero.
  \item Regla 4: Si el angulo es Cero \textbf{y} la velocidad angular es Negativa Alta \textbf{entonces} la velocidad sera Negativa Alta.
  \item Regla 5: Si el angulo es Cero \textbf{y} la velocidad angular es Negativa Baja \textbf{entonces} la velocidad sera Negativa Baja.
  \item Regla 6: Si el angulo es Cero \textbf{y} la velocidad angular es Cero \textbf{entonces} la velocidad sera Cero.
  \item Regla 7: Si el angulo es Cero \textbf{y} la velocidad angular es Positiva Baja \textbf{entonces} la velocidad sera Positiva Baja.
  \item Regla 8: Si el angulo es Cero \textbf{y} la velocidad angular es Positiva Alta \textbf{entonces} la velocidad sera Positiva Alta.
  \item Regla 9: Si el angulo es Positivo Bajo \textbf{y} la velocidad angular es Negativa Baja \textbf{entonces} la velocidad sera Cero.
  \item Regla 10: Si el angulo es Positivo Bajo \textbf{y} la velocidad angular es Cero \textbf{entonces} la velocidad sera Positiva Baja.
  \item Regla 11: Si el angulo es Positivo Alto \textbf{y} la velocidad angular es Cero \textbf{entonces} la velocidad sera Positiva Alta.
\end{itemize}

Para entender el funcionamiento del sistema calcularemos la salida de la entrada de la Figura~\ref{fig:fuzzycontrolinput}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.6\textwidth]{angulo}
  \includegraphics[width=0.6\textwidth]{velocidadang}
  \caption{Entrada del sistema de control difuso.}
  \label{fig:fuzzycontrolinput}
\end{figure}

Si tenemos un valor entre 0 y a para el angulo con respecto a la vertical y un valor entre -a y 0 para la velocidad angular tenemos que ese valor de angulo se corresponde con los conjuntos difusos Cero y Positivo Bajo mientras que la velocidad angular pertenece en cierta cantidad a los conjuntos Cero y Negativo Bajo. Debido a esto se pueden aplicar las siguientes reglas:

\begin{itemize}
  \item Regla  5: $C \wedge NB \rightarrow NB$
  \item Regla  6: $C \wedge C \rightarrow C$
  \item Regla  9: $PB \wedge NB \rightarrow PB$
  \item Regla 10: $PB \wedge C \rightarrow C$
\end{itemize}

En la Figura~\ref{fig:regla6} podemos ver el resulta de aplicar la regla 6. Como se puede ver en lógica difusa el \emph{AND} se calcula como el mínimo de elementos que conforman el AND.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.6\textwidth]{regla6}
  \caption{Calculo del resultado de aplicar la regla 6.}
  \label{fig:regla6}
\end{figure}

El resultado de aplicar las reglas 5, 9 y 10 se pueden ver en la Figura~\ref{fig:otherreglas}.

Hay que destacar que en la documentación proporcionada una de las reglas aplicadas tiene un error, la regla "\emph{Si el ángulo es cero y la velocidad angular es positiva baja...}" no se puede aplicar en este caso. La regla que si que se puede aplicar es la regla 10 que no se contempla en la documentación.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.3\textwidth]{regla5}
  \includegraphics[width=0.3\textwidth]{regla9}
  \includegraphics[width=0.3\textwidth]{regla10}
  \caption{Resultado de aplicar el resto de reglas.}
  \label{fig:otherreglas}
\end{figure}

Si solapamos los resultados obtendremos un resultado único, que tendrá que ser \emph{desfuzzyficado}. El algoritmo usado en esta ocasión para hacer la desfuzzyficación es el del centro de gravedad. El resultado se puede ver en la Figura~\ref{fig:result}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{result}
  \caption{Resultado final.}
  \label{fig:result}
\end{figure}


\newpage
\section{Análisis crítico}

El lenguaje humano es inherentemente ambiguo por tanto el conocimiento expresado con lenguaje humano es muchas veces ambiguo. Etiquetas semánticas como \emph{bastante}, \emph{mucho} o poco son constantes en el conocimiento humano. Sin embargo es complicado expresar este tipo de reglas de forma categórica. Lo que es mucho en un contexto puede ser poco en un contexto totalmente distinto y viceversa.

La lógica difusa permite lidiar con este tipo de ambigüedad de forma muy sencilla y traducir conocimiento humano a sistemas implementables con relativa facilidad. Ademas las reglas de un sistema de control difuso pueden ser formuladas con bastante sencillez por un experto en el sistema. Se puede hacer que el experto indique en una escala de -1 a 1 un valor de forma que evitamos las etiquetas semánticas y conseguimos mejores resultados.

Con respecto al planteamiento del problema uno de los detalles importantes a destacar es la ausencia de multitud de reglas. Muchas de las combinaciones de angulo con respecto a la vertical y velocidad angular no están contempladas en la propuesta por lo que en caso de que se den estas situaciones el sistema no tendría definida ninguna acción.




\newpage
\appendix
\begin{thebibliography}{99}

\bibitem{apuntes}
  Vicente Moret et al.,
  \emph{Fundamentos de Inteligencia Artificial}.
  Servicio de Publicaciones UDC. 3a edición.2004.

\bibitem{proporcionada}
  Peter Bauer et al.,
  \emph{Lógica borrosa y Control Borroso del Péndulo Invertido}.
  \url{http://proton.ucting.udg.mx/posgrado/cursos/idc/logica/fuzzy/index.html}

\bibitem{wiki:invertedpendulum}
  Wikipedia,
  \emph{Inverted Pendulum}.
  \url{http://en.wikipedia.org/wiki/Inverted\_pendulum}.

\bibitem{wiki:segway}
  Wikipedia,
  \emph{Segway PT}
  \url{http://en.wikipedia.org/wiki/Segway\_PT}

\bibitem{wiki:controlthe}
  Wikipedia,
  \emph{Control theory}.
  \url{http://en.wikipedia.org/wiki/Control\_theory}.

\bibitem{wiki:fuzlogic}
  Wikipedia,
  \emph{Fuzzy logic}.
  \url{http://en.wikipedia.org/wiki/Fuzzy\_logic}.

\bibitem{wiki:defuz}
  Wikipedia,
  \emph{Defuzzification}.
  \url{http://en.wikipedia.org/wiki/Defuzzification}.

\bibitem{}
  Michael J. Watts,
  \emph{Fuzzy Inference and Defuzzification}.
  \url{http://mike.watts.net.nz/Teaching/IIS}.

\bibitem{centrodegravedad}
  Wikipedia,
  \emph{Centro de gravedad}.
  \url{http://es.wikipedia.org/wiki/Centro\_de\_gravedad}


\end{thebibliography}

\end{document}
